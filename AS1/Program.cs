﻿using AS1;
class Program
{
	static void Main(string[] args)
	{

		List<GiaoVien> giaoViens = new List<GiaoVien>();
		//Console.Write("Nhap so luong giao vien: ");
		//int soLuong = int.Parse(Console.ReadLine());
		int soLuong = Validate.inputInt("Nhap so luong giao vien: ", 1, 20);
		for (int i = 1; i <= soLuong; i++)
		{
			Console.WriteLine($"NHAP THONG TIN GIAO VIEN THU {i}: ");
			//Console.Write("Ho ten: ");
			//string hoTen = Console.ReadLine();
			string hoTen = Validate.inputString("Ho ten: ", 1, 50);
			//Console.Write("Nam sinh: ");
			//int namSinh = int.Parse(Console.ReadLine());
			//Console.Write("Luong co ban: ");
			//double luongCoBan = double.Parse(Console.ReadLine());
			//Console.Write("He so luong: ");
			//double heSoLuong = double.Parse(Console.ReadLine());
			int namSinh = Validate.inputInt("Nam sinh: ", 999, 4000);
			double luongCoBan = Validate.inputDou("Luong co ban: ", 0, 999999999999);
			double heSoLuong = Validate.inputDou("He so luong: ", 0, 999999);
			GiaoVien giaoVien = new GiaoVien(hoTen, namSinh, luongCoBan, heSoLuong);
			giaoViens.Add(giaoVien);
		}


		GiaoVien giaoVienLuongThapNhat = giaoViens.OrderBy(gv => gv.TinhLuong()).FirstOrDefault();
		if (giaoVienLuongThapNhat != null)
		{
			Console.WriteLine("Thong tin giao vien co luong thap nhat: ");
			giaoVienLuongThapNhat.XuatThongTin();
		}
		else
		{
			Console.WriteLine("Khong co giao vien nao trong danh sach");
		}
		Console.Read();
	}
}