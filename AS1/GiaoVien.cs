﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AS1
{
	internal class GiaoVien : NguoiLaoDong
	{
		protected double HeSoLuong { get; set; }

		public GiaoVien()
		{
		}

		public GiaoVien(string hoTen, int namSinh, double luongCoBan, double heSoLuong) : base(hoTen, namSinh, luongCoBan)
		{
			HeSoLuong = heSoLuong;
		}
		public void NhapThongTin(int heSoLuong)
		{
			HeSoLuong = heSoLuong;
		}
		public override double TinhLuong()
		{
			return LuongCoBan * HeSoLuong * 1.25;
		}
		public override void XuatThongTin()
		{
			Console.WriteLine("Ho ten la: " + HoTen + ", nam sinh: " + NamSinh + ", luong co ban: " + LuongCoBan + ", he so luong: " + HeSoLuong + ", luong : " + TinhLuong() + ".");

		}
		public void XuLy()
		{
			HeSoLuong += 0.6;
		}
	}
}
