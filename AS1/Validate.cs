﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AS1
{
	public class Validate
	{
		private static Boolean check = false;
		
		public static int inputInt (string mgs, int min, int max)
		{
			int flag;
			if (min > max)
			{
				flag = min;
				min = max;
				max = flag;

			}
			int data;
			check = false;
			do
			{
				Console.Write(mgs);
				string input = Console.ReadLine();
				check = int.TryParse(input, out data);
				if (!check) {
					Console.WriteLine("vui long nhap lai!");
				}
			} while (data < min || data > max || !check);
			return data;

		}
		public static double inputDou(string mgs, double min, double max)
		{
			double flag;
			if (min > max)
			{
				flag = min;
				min = max;
				max = flag;

			}
			double data;
			check = false;
			do
			{
				Console.Write(mgs);
				string input = Console.ReadLine();
				check = double.TryParse(input, out data);
				if (!check)
				{
					Console.WriteLine("vui long nhap lai!");
				}
			} while (data < min || data > max || !check);
			return data;

		}
		public static string inputString(string mgs, int min, int max)
		{
			int flag;
			if (min > max)
			{
				flag = min;
				min = max;
				max = flag;

			}
			string data;
			check = false;
			do
			{
				Console.Write(mgs);
				string input = Console.ReadLine();
				check = input.Length > 0 || input != null;
				data = input;
				
				if (!check)
				{
					Console.WriteLine("vui long nhap lai!");
				}
				
			} while (data.Length < min || data.Length > max || !check);
			return data;

		}
	}
}
