﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AS1
{
	internal class NguoiLaoDong
	{
		protected String HoTen { get; set; }
		protected int NamSinh { get; set; }
		protected double LuongCoBan { get; set; }

		public NguoiLaoDong(string hoTen, int namSinh, double luongCoBan)
		{
			HoTen = hoTen;
			NamSinh = namSinh;
			LuongCoBan = luongCoBan;
		}
		public NguoiLaoDong() { }
		public void NhapThongTin(string hoTen, int namSinh, double luongCoBan)
		{
			HoTen = hoTen;
			NamSinh = namSinh;
			LuongCoBan = luongCoBan;
		}
		public virtual double TinhLuong()
		{
			return LuongCoBan;
		}
		public virtual void XuatThongTin()
		{
			Console.WriteLine("Ho ten la: " + HoTen + ", nam sinh: " + NamSinh + ", luong co ban: " + LuongCoBan + ".");
		}
	}
}
